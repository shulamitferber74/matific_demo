import { Component } from '@angular/core';

@Component({
  selector: 'main-area',
  templateUrl: './main.html'
})

export class MainComponent {
  title = 'app';
}
