import { Injectable } from '@angular/core';
import { UsersService } from './users.service';
@Injectable()

export class AuthenticationService{
    
   _isAuth=false;
    
    constructor(private users:UsersService){
        
        
    }
    
    isAuth(){
        
        return (this.users.getCurrentUser() && this.users.getCurrentUser()!=null);
    }
    
    checkAuth(username:string,password:string):boolean{
        
        return this.users.getUser(username,password);
        
        
        
    }
    
    
    
}