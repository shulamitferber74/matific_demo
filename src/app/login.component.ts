import { Component } from '@angular/core';

import { FormControl, FormControlName, FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';

import { AuthenticationService } from './services/auth.service';

import { UsersService } from './services/users.service';

import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'login-area',
  templateUrl: 'login.html',
    styleUrls:['../css/style.css']
})

export class LoginComponent {

  title = 'app';

  loginForm: FormGroup;

  loginStatus = '';

  constructor(public formBuilder: FormBuilder, private auth: AuthenticationService, private users: UsersService,public router:Router) { // declares form builder here


      /*this.router.events.subscribe((event)=>{
          
          if (event instanceof NavigationEnd) {
              
              
                                              
          }
      })*/
  }

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }


    logout(event){
        
        this.users.clearCurrentUser();
        
        this.router.navigate(['/main']);
    }
  login(event) {


    if (this.auth.checkAuth(this.loginForm.controls['username'].value, this.loginForm.controls['password'].value)) {

      this.users.setCurrentUser(this.loginForm.controls['username'].value)

    console.log("current user:"+this.users.getCurrentUser())
        
    this.router.navigate(['/dashboard']);
        
        
    }
    else {

      this.loginStatus = 'password and username did not match!'
    }

  }


}


