import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from './services/auth.service';

import { UsersService } from './services/users.service';

import { Router, NavigationEnd} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../css/style.css']

})
export class AppComponent implements OnInit {

  public isLogged = false;

  public userName = "guest";

  public notify = 'please login or register!'

  constructor(private auth: AuthenticationService, private users: UsersService, private router: Router) {

    router.events.subscribe((event) => {

      if (event instanceof NavigationEnd) {

        this.userName = this.users.getCurrentUser() || "guest"
          
          this.isLogged=this.users.getCurrentUser()!=null;

      }
    })
  }

  ngOnInit(): void {

      this.isLogged=this.users.getCurrentUser()!=null;
  }
    
    clearUser(){
        
        this.users.clearCurrentUser();
        
        this.userName = "guest";
        
        this.isLogged=false;
        
        this.router.navigate(['/main']);
        
    }


}

