import { Injectable } from '@angular/core';


@Injectable()

export class UsersService{
    
    constructor(){
        
        
    }
    
    
    
    addUser(username:string,password:string){
        
        if (!localStorage.getItem('matific') || localStorage.getItem('matific') == null) {
        
        localStorage.setItem('matific', JSON.stringify({'users':[]}))
        
        
        }
        
        var arr=JSON.parse(localStorage.getItem('matific')); 
    
        arr.users.push( {name:username,pass:password})
        
        localStorage.setItem('matific', JSON.stringify(arr))
        
        return true;
    
    }
    
    removeUser(username:string){
        
        if (!localStorage.getItem('matific') || localStorage.getItem('matific') == null) 
            return;
        
        var obj= JSON.parse(localStorage.getItem('matific'));
        
        var users=obj.users;
        
        for(var i=0;i<users.length;i++){
            
            if(users[i].name==username){
                
                users.splice(i,1);
                
                localStorage.setItem('matific', JSON.stringify(obj))
                
                break;
            
            }
        }  
    }
    
    
    setCurrentUser(username:string){
         
        if (!localStorage.getItem('matific') || localStorage.getItem('matific') == null) 
            return false;
        
        var obj=JSON.parse(localStorage.getItem('matific'));
        
        obj.currentUser=username;
        
        localStorage.setItem('matific', JSON.stringify(obj))
        
    }
    
    clearCurrentUser(){
         
        if (!localStorage.getItem('matific') || localStorage.getItem('matific') == null) 
            return false;
        
        var obj=JSON.parse(localStorage.getItem('matific'));
        
        obj.currentUser=null;
        
        localStorage.setItem('matific', JSON.stringify(obj))
        
    }
    
    getCurrentUser(){
        
        if (!localStorage.getItem('matific') || localStorage.getItem('matific') == null) 
            return null;
        
         var obj=JSON.parse(localStorage.getItem('matific'));
        
        console.log(obj)
        
        return obj.currentUser;
    }
        
    getUser(username:string,password:string){
        
        if (!localStorage.getItem('matific') || localStorage.getItem('matific') == null) 
            return;
        
        var users=JSON.parse(localStorage.getItem('matific')).users;
        
        console.log(users)
        
        for(var i=0;i<users.length;i++){
            
            if(users[i].name==username && users[i].pass==password){
                return true;
            
            }
        }
        
        return false;
    }
    
    getAllUsers(){
        
        return JSON.parse(localStorage.getItem('matific')).users;
        
    }
    
    clearUsers(){
        
        localStorage.setItem('matific',JSON.stringify({'users':[]}));
    }
    
    
    
}