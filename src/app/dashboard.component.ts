import { Component,OnInit } from '@angular/core';

import { DataService } from './services/data.service';

import { FormsModule } from '@angular/forms';

import {orderByPipe} from './pipes/orderBy.pipe';

import {orderWordPipe} from './pipes/orderWord.pipe';

import {AchievementComponent} from './directives/AchievementBar.component'

@Component({
  selector: 'dashboard-area',
  templateUrl: './dashboard.html',
    styleUrls: ['dashboard.css']
})

export class DashboardComponent {
    
    title = 'app';
    
    parameters=[{name:'date',label:'Date Completed'},{name:'content',label:'Content'},{name:'type',label:'Type'},{name:'skill',label:'Skill'},{name:'result','label':'Result'},{name:'time',label:'Time Spent'}]

    classes;
    
    students=[]
    
    activities;
    
    studentActivities=[];
    
    sortOrder=1;
    
    currentFilter='date';
    
    date1='2016-01-01';
    
    date2='2017-09-01';
    
    currentStudent:String="All";

    constructor(private data: DataService) {


    }
    
    ngOnInit(){
        
        this.loadData()
    }

 classChange(val){
    
        var studentList=this.classes.filter(_class => {return _class.name==val});
    
        if(studentList!=null && studentList.length>0){
            
            this.students=studentList[0]['students']
            
            this.studentChange('All')
            
        }
       
    }
    
    studentChange(val=null){
     
        if(val!=null)
            this.currentStudent=val;     
    
        this.studentActivities=this.activities.filter(activity=>{
            
            if(this.currentStudent=="All"){ // if all student are selected , return results for all students name who match the current class
                
                return(this.students.indexOf(activity.student)>-1 && this.checkDate(activity.date))
            }
            
            
            return (activity.student==this.currentStudent && this.checkDate(activity.date)) // else filter the results by student name
        
        }) 
          
    }
    
        
    checkDate(date){
        
        //console.log(this.date1,date,this.date2)
        
        return (new Date(date) >=new Date(this.date1) && new Date(date) <=new Date(this.date2))
    }
      
    
   endDateChange(val){
        
        this.date2=val;
       
       this.studentChange()
    }
    
     startDateChange(val){
        
        this.date1=val;
         
         this.studentChange()
         
     
     }
    
    filterBy(key){
        
        if(this.currentFilter==key){
            
            this.sortOrder*=-1;
            
            return;
        
        }
        
        this.currentFilter=key;
        
        this.sortOrder=1;
        
        
    }
    
  loadData() {
      
    this.data.getClassesList().then((result) => {

        this.classes=result.classes;
        
        //console.log(result)
        this.onLoadComplete()
        
    })

    this.data.getActivitiesList().then((result) => {

        this.activities=result.data;
        
        console.log(this.activities)
        
        this.onLoadComplete()
        
    })

  }
    
    onLoadComplete(){
        
        if(this.activities!=null && this.classes!=null){
            
            this.classChange(this.classes[0].name)
            
            this.studentChange('All')
        }
            
    }
}

