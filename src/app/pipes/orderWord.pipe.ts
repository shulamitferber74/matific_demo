
    import {Pipe, PipeTransform} from '@angular/core';
  
//accepts an array of strings that each may contain several words , orders the strings by selected word idx.

//['Aword Bword','Cword Aword']. orderBy:0 => ['Aword Bword','Cword Aword'] orderBy:1 => ['Cword Aword','Aword Bword']
   
@Pipe({name: 'orderByWord'})
    export class orderWordPipe implements PipeTransform {
      transform(arr,wordIdx:number,splitBy:string,order:number) : any {
      
            return arr.sort((a,b)=>{
            
                var key1=a.split(splitBy)[wordIdx]
                
                var key2=b.split(splitBy)[wordIdx]
                
            if(!key1 || !key2)
                return 0;
                
            return key1>key2 ? order*1: order*-1; 
        })
    }
     
      }
    