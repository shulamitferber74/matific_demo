import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import 'rxjs/add/operator/map';
@Injectable()

export class DataService{
    
    private classesUrl='https://ljifg6p8cd.execute-api.us-east-1.amazonaws.com/production/matific-test-classes'
    
    private activitiesUrl='https://ljifg6p8cd.execute-api.us-east-1.amazonaws.com/production/matific-test-activities'
   
    private classes;
    
    private activities;
    
    constructor(private http:Http){
        
        
    }
    
    getActivitiesList(){
        
        return this.http.get(this.activitiesUrl).map((response)=> response.json()).toPromise();
        
    };
    
    getClassesList(){
       
        return this.http.get(this.classesUrl).map((response) => response.json()).toPromise();
        
    };
    
} 