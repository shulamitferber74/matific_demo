
    import {Pipe, PipeTransform} from '@angular/core';
  
//orders an aray with keys by a given key 

    @Pipe({name: 'toFixed'})
    export class toFixedPipe implements PipeTransform {
      transform(num, places:number) : number {
      
          console.log(num,places)
             return num.toFixed(places)
     
      }}
    