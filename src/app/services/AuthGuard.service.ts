
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthenticationService,private router:Router) {}

  canActivate() {
      
      
   if(this.auth.isAuth()){
       
       return true;
  }
      else{
          
          console.log('-- redirected to login!--')
          
          this.router.navigate(['/login']);
          
          return false;
      }
}}