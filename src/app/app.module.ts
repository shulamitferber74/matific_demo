import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { MainComponent } from './main.component';

import { DashboardComponent } from './dashboard.component';

import { LoginComponent } from './login.component';

import { RegisterComponent } from './register.component';

import { DataService } from './services/data.service';

import { AuthGuard } from './services/AuthGuard.service';

import { AuthenticationService } from './services/auth.service';

import { RegisterService } from './services/register.service';

import { UsersService } from './services/users.service';

import { orderByPipe } from './pipes/orderBy.pipe';

import { orderWordPipe } from './pipes/orderWord.pipe';

import { toFixedPipe } from './pipes/toFixed.pipe';

import { Headers, HttpModule } from '@angular/http';

import {ReactiveFormsModule } from '@angular/forms';

import {FormsModule } from '@angular/forms';

import {AchievementComponent} from './directives/AchievementBar.component'


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'main' },
  { path: 'main', component: MainComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
];



@NgModule({
  declarations: [
    AppComponent, MainComponent, DashboardComponent, LoginComponent, RegisterComponent,orderByPipe,orderWordPipe,toFixedPipe,AchievementComponent
  ],
  imports: [
    BrowserModule, HttpModule, FormsModule,ReactiveFormsModule, RouterModule.forRoot(routes,{ useHash: true })],

  providers: [DataService, AuthenticationService, AuthGuard, UsersService, RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
