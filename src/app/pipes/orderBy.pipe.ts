
    import {Pipe, PipeTransform} from '@angular/core';
  
//orders an aray with keys by a given key 

    @Pipe({name: 'orderBy'})
    export class orderByPipe implements PipeTransform {
      transform(arr, key:string,order:number) : any {
      
             return arr.sort((a,b)=>{
            
            var key1;
            
            var key2;
            
            if(a[key]==null || b[key]==null)
                return 0;
            
            switch(key){
                    
                case 'date':
                    key1=new Date(a[key])
                 
                    key2=new Date(b[key])
                   
                    break;
                    
                case 'time':
                   
                    key1=this.createTime(a[key])
                    
                    key2=this.createTime(b[key])
                    
                    break;
                    
                default:
                    
                    key1=a[key];
                    
                    key2=b[key];
                    
                    break;
                    
            }
           
            //console.log(key1,key2)
            return key1>key2 ? order*1: order*-1; 
        })
    }
    
    createTime(a){
        
        var hours=0;
        
        var minutes=0;
        
        var minutesArr;
        
        if(a.indexOf('hr')>-1){
        
            hours=Number(a.split('hr')[0])
            
        }
        
        if(a.indexOf('hr ')>-1){
            
            minutesArr=a.split('hr ')[1]
        }
        else{
            
            minutesArr=a;
            
        }
                         
        if(minutesArr.indexOf('m')>-1){
        
            minutes=Number(minutesArr.split('m')[0])
            
        }                 
        
        
        return hours*60+minutes;
    }
    
    
     
      }
    