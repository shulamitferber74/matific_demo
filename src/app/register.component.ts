import { Component, ViewChild, OnInit } from '@angular/core';

import { FormControl, FormControlName, FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';

import { UsersService } from './services/users.service';

import { Router } from '@angular/router';

@Component({
  selector: 'register-area',
  templateUrl: './register.html',
   styleUrls:['../css/style.css']
})
export class RegisterComponent {

  title = 'app';

  registerForm: FormGroup;
    
 registerStatus='';

  constructor(public formBuilder: FormBuilder, private users: UsersService,public router:Router) { // declares form builder here


      this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      password_repeat: ['', Validators.required]
    })
  }

  ngOnInit() {

    

  }


  register() {

      var fields=this.registerForm.controls;
      
      this.users.addUser(fields['username'].value, fields['password'].value)
      
    if (!fields['username'].value || !fields['password'].value ||  !fields['password_repeat'].value) {

         this.registerStatus='please fill all the fields!'
        
    }
      
   else if (fields['password'].value != fields['password_repeat'].value) {

         this.registerStatus='your passwords did not match!'
        
    }
    else {
      if (this.users.addUser(fields['username'].value, fields['password'].value)) {

        console.log("registerd successfully")
          
          this.users.setCurrentUser(fields['username'].value)
          
          this.router.navigate(['/dashboard']);
      }


    }

  }
    
        clearUsers(){
    
    this.users.clearUsers()
}
}
