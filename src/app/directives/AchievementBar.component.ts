import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { toFixedPipe } from '../pipes/toFixed.pipe';

@Component({
  selector: 'achievement-bar',
  templateUrl: 'achievementBar.html',
  styleUrls: ['achievementBar.css']
})

export class AchievementComponent {

  static LOW_BAR = 55;

  static HIGH_BAR = 90;

  @Input() data;

  low = 0;

  medium = 0;

  high = 0;
    
    total=0;

  constructor() {


  }

  //listen for changes after instantiation
  public ngOnChanges(changes) {

    this.total = this.data.length;

   this.low = 0;

    this.medium = 0;

    this.high = 0;

    for (var i = 0; i < this.data.length; i++) {

      if (this.data[i].result <= AchievementComponent.LOW_BAR)
        this.low++
      else if (this.data[i].result >= AchievementComponent.LOW_BAR && this.data[i].result < AchievementComponent.HIGH_BAR) {
        this.medium++
      }
      else {
        this.high++

      }

    }

   // console.log(this.total, this.low, this.medium, this.high)

  }
}
